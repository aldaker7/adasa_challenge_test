from django.db import models

# Create your models here.


class tbl_mast_meteo_station(models.Model):
    name = models.CharField(max_length=255, null=False)
    longitude = models.FloatField(null=False)
    latitude = models.FloatField(null=False)

    def __str__(self):
        return self.id, self.name

class tbl_mast_meteo_variable(models.Model):
    name = models.CharField(max_length=45, null=False)
    unit = models.CharField(max_length=45, null=False)

    def __str__(self):
        return self.name
