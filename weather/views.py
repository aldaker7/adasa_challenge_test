from django.shortcuts import render
import requests, json
from weather import models
from django.http import HttpResponse
import calendar
from django.core.paginator import Paginator

# Create your views here.


def get_index(request):
    """
    TO REDIRECT to first METRO
    """
    return get_temperature(request,'1','1')


def get_temperature(request, metro_id, page):
    """
    This Method to call GET api from a mockable website
    :param metro_id:
    :param page_id:
    :return: list of values for metro1,2,3 or 4
    """
    if metro_id is None:
        metro_id = 1
    header = {"Content-Type": "application/json"}
    req = requests.get('https://demo4062187.mockable.io/meteo', data='', headers=header)
    if req.status_code in [200,201]:
        metro = []
        labels_date = ''
        tempera = ""
        content = req.json()
        for i in content:
            if i.get('idStation') == int(metro_id):
                for var in i.get('variables'):
                    obj = models.tbl_mast_meteo_variable.objects.get(pk=var.get('id'))
                    var["name"] = obj.name
                    var["unit"] = obj.unit

                    if obj.name == 'Temperature':
                        tempera += str(round(float(var.get('value'))))+","
                        month = calendar.month_name[int(i.get('date').split('/')[1])][:3]
                        labels_date += i.get('date').split('/')[0]+" " + month+" " + ','
                metro.append(i)
        pageinator = Paginator(metro,10)

        return render(request,'index.html',context={'metro': pageinator.get_page(page),
                                                     "my_data": tempera,
                                                     "dates": labels_date,
                                                     'metro_id': metro_id,
                                                     'page': pageinator.get_page(page)})
    else:
        return HttpResponse("No Data Available")



